<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorldBossesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('worldbosses', function($newtable)
        {
            $newtable->increments('id');
            $newtable->string('name');
            $newtable->string('zone');
            $newtable->string('area');
            $newtable->string('waypoint');
            $newtable->timestamps();

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('worldbosses');
	}

}
