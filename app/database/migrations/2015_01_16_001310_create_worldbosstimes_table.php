<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorldbosstimesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('worldbosstimes', function($newtable)
        {
            $newtable->increments('id');
            $newtable->integer('worldBoss_id')->unsigned();
            $newtable->foreign('worldBoss_id')->references('id')->on('worldbosses');
            $newtable->integer('hour');
            $newtable->integer('minute');
            $newtable->timestamps();

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('worldbosstimes');
	}

}
