<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('pages', function($newtable)
        {
            $newtable->increments('id');
            $newtable->string('name');
            $newtable->text('content');
            $newtable->boolean('active')->default(0);
            $newtable->boolean('homepage')->default(0);
            $newtable->timestamps();

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pages');
	}

}
