<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePathsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('paths', function($newtable)
        {
            $newtable->increments('id');
            $newtable->integer('dungeon_id')->unsigned();
            $newtable->foreign('dungeon_id')->references('id')->on('dungeons');
            $newtable->string('name');
            $newtable->string('tag');
            $newtable->string('description');
            $newtable->timestamps();

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('paths');
	}

}
