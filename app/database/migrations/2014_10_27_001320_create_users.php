<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsers extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function($newtable)
        {
            $newtable->increments('id');
            $newtable->string('email')->unique;
            $newtable->string('displayname', 100)->unique;
            $newtable->string('password', 128);
            $newtable->integer('achievement_points');
            $newtable->integer('rank_id')->unsigned();
            $newtable->foreign('rank_id')->references('id')->on('ranks');
            $newtable->integer('role_id')->unsigned();
            $newtable->foreign('role_id')->references('id')->on('roles');
            $newtable->string('remember_token', 100);
            $newtable->string('confirmation_code')->nullable();
            $newtable->boolean('active')->default(0);
            $newtable->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }

}
