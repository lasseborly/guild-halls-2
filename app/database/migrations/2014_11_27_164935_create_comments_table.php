<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('comments', function($newtable)
        {
            $newtable->increments('id');
            $newtable->integer('user_id')->unsigned();
            $newtable->foreign('user_id')->references('id')->on('users');
            $newtable->integer('creator_id')->unsigned();
            $newtable->foreign('creator_id')->references('id')->on('users');
            $newtable->string ('comment');
            $newtable->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('comment');
	}

}
