<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharactersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('characters', function($newtable)
        {
            $newtable->increments('id');
            $newtable->integer('user_id')->unsigned();
            $newtable->foreign('user_id')->references('id')->on('users');
            $newtable->string('name');
            $newtable->integer('level')->default(1);
            $newtable->integer('profession_id')->unsigned();
            $newtable->foreign('profession_id')->references('id')->on('professions');
            $newtable->integer('race_id')->unsigned();
            $newtable->foreign('race_id')->references('id')->on('races');
            //Sex: 0 = male, 1 = female
            $newtable->boolean('sex')->default(0);
            $newtable->boolean('personal_story')->default(0);
            $newtable->boolean('world_completion')->default(0);
            $newtable->integer('order_id')->unsigned();
            $newtable->foreign('order_id')->references('id')->on('orders');
            $newtable->integer('fractal_level')->default(0);
            $newtable->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('characters');
	}

}
