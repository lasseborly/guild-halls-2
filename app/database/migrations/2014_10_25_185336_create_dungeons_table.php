<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDungeonsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('dungeons', function($newtable)
        {
            $newtable->increments('id');
            $newtable->string('name');
            $newtable->string('tag');
            $newtable->string('description');
            $newtable->timestamps();

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dungeons');
	}

}
