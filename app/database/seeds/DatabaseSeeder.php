<?php

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Eloquent::unguard();
        $this->call('RankTableSeeder');
        $this->call('RoleTableSeeder');
        $this->call('RaceTableSeeder');
        $this->call('ProfessionTableSeeder');
        $this->call('OrderTableSeeder');
        $this->call('GuildTableSeeder');
        $this->call('DungeonTableSeeder');
        $this->call('PathTableSeeder');
        $this->call('UserTableSeeder');
        $this->call('CommentTableSeeder');
        $this->call('CharacterTableSeeder');
        $this->call('PageTableSeeder');
        $this->call('WorldBossTableSeeder');
        $this->call('WorldBossTimesTableSeeder');

    }

}

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->insert([
            'email'      => 'lasseborly@gmail.com',
            'displayname'=> 'JoinerJack.9205',
            'password'   => Hash::make('DKLAnders3'),
            'achievement_points' => 6000,
            'rank_id' => 2,
            'role_id' => 1,
            'remember_token' => '0',
            'confirmation_code' => '0',
            'active' => 1,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('users')->insert([
            'email'      => 'kingbert@gmail.com',
            'displayname'=> 'King Bert.7369',
            'password'   => Hash::make('DKLAnders3'),
            'achievement_points' => 3793,
            'rank_id' => 3,
            'role_id' => 2,
            'remember_token' => '0',
            'confirmation_code' => '0',
            'active' => 1,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('users')->insert([
            'email'      => 'zoe@gmail.com',
            'displayname'=> 'TopazTaylor.2791',
            'password'   => Hash::make('DKLAnders3'),
            'achievement_points' => 10580,
            'rank_id' => 2,
            'role_id' => 2,
            'remember_token' => '0',
            'confirmation_code' => '0',
            'active' => 1,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('users')->insert([
            'email'      => 'dybt@gmail.com',
            'displayname'=> 'dybt.3789',
            'password'   => Hash::make('DKLAnders3'),
            'achievement_points' => 3675,
            'rank_id' => 2,
            'role_id' => 2,
            'remember_token' => '0',
            'confirmation_code' => '0',
            'active' => 1,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

    }
}

class RankTableSeeder extends Seeder {

    public function run()
    {
        DB::table('ranks')->insert([
            'rank' => 'Founder',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('ranks')->insert([
            'rank' => 'Leader',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('ranks')->insert([
            'rank' => 'Second Commander',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('ranks')->insert([
            'rank' => 'Knight',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('ranks')->insert([
            'rank' => 'Squire',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('ranks')->insert([
            'rank' => 'Soldier',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('ranks')->insert([
            'rank' => 'Recruit',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

    }
}

class DungeonTableSeeder extends Seeder {

    public function run()
    {
        DB::table('dungeons')->insert([
            'name' => 'Ascalonian Catacombs',
            'tag' => 'AC',
            'description' => 'Eir Stegalkin, the norn hero, has gone into these ancient, ghost-haunted catacombs.
            Rytlock Brimstone wants you to go after her, before she stirs up King Adelberns long-dead citizens',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('dungeons')->insert([
            'name' => 'Caudecus Manor',
            'tag' => 'CM',
            'description' => 'Legate Minister Caudecus is hosting a party to celebrate the human-charr détente in his home, Beetletun Manor.
            While a vocal opponent of Queen Jennah, Caudecus swears his loyalty to Kryta. The minister promises to unveil a new creation by his personal inventor, Uzolan',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('dungeons')->insert([
            'name' => 'Twillight Arbor',
            'tag' => 'TA',
            'description' => 'The Nightmare Court consists of sylvari who turned their backs on the Pale Tree and sought their own cruel purposes in life.
            Their leader, Grand Duchess Faolain, makes her lair in the Twilight Arbor, surrounded by loyal minions. They have been kidnapping Sylvari for their own nefarious purposes',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('dungeons')->insert([
            'name' => 'Sorrows Embrace',
            'tag' => 'SE',
            'description' => 'The dredge have claimed all former lands of the dwarves.
            What was once Sorrows Furnace, a place of dredge enslavement, is now known as Sorrows Embrace.
            Great peril may still be found within these depths',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('dungeons')->insert([
            'name' => 'Citadel of Flame',
            'tag' => 'CoF',
            'description' => 'The Flame Citadel is the main base of the Flame Legion, the deposed former legion of the charr.
            It is here that Gaheron Baelfire plots to retake command of the charr and crush the humans',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('dungeons')->insert([
            'name' => 'Honor of the Waves',
            'tag' => 'HoW',
            'description' => 'The Elder Dragon Jormag has driven the kodan south in their great sanctuary ships.
            Now, the ship called the Honor of the Waves lies stricken and sinking from an assault from the dragons minions',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('dungeons')->insert([
            'name' => 'Crucible of Eternity',
            'tag' => 'CoE',
            'description' => 'The Inquest is seeking to use the power of the Elder Dragons themselves in their desire to dominate the other races.
            Unless they are stopped, they endanger all of Tyria with their maniacal greed',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('dungeons')->insert([
            'name' => 'The Ruined City of Arah',
            'tag' => 'Arah',
            'description' => 'The members of Destinys Edge have reunited.
            They now need to get an airship back into the fight in order to join the assault on the Elder Dragon Zhaitan.
            The war against the undead Elder Dragon resolves as every resource is used over the skies of the City of the Human Gods',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

    }
}

class PathTableSeeder extends Seeder {

    public function run()
    {
        DB::table('paths')->insert([
            'dungeon_id' => '1',
            'name' => 'Hodgins plan',
            'tag' => '1',
            'description' => 'Collect the flaming scepters to defeat the Howling King',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('paths')->insert([
            'dungeon_id' => '1',
            'name' => 'Dethas plan',
            'tag' => '2',
            'description' => 'Trap and kill the Ghost Eater',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('paths')->insert([
            'dungeon_id' => '1',
            'name' => 'Tzarks plan',
            'tag' => '3',
            'description' => 'Get the ghosts to help fight Colossus Rumblus',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('paths')->insert([
            'dungeon_id' => '2',
            'name' => '',
            'tag' => '1',
            'description' => 'Ill help the visiting asura',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('paths')->insert([
            'dungeon_id' => '2',
            'name' => '',
            'tag' => '3',
            'description' => 'Ill look for the Seraph',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('paths')->insert([
            'dungeon_id' => '2',
            'name' => '',
            'tag' => '3',
            'description' => 'Ill help you find the missing butler',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('paths')->insert([
            'dungeon_id' => '3',
            'name' => 'Forward',
            'tag' => 'FW',
            'description' => 'Lets keep moving forward',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('paths')->insert([
            'dungeon_id' => '3',
            'name' => 'Up',
            'tag' => 'UP',
            'description' => 'Lets go up',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('paths')->insert([
            'dungeon_id' => '3',
            'name' => 'Aetherpath',
            'tag' => 'AE',
            'description' => 'Ill take down the Aetherblades',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('paths')->insert([
            'dungeon_id' => '4',
            'name' => 'Fergg.',
            'tag' => '1',
            'description' => '',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('paths')->insert([
            'dungeon_id' => '4',
            'name' => 'Rasolov.',
            'tag' => '2',
            'description' => '',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('paths')->insert([
            'dungeon_id' => '4',
            'name' => 'Koptev.',
            'tag' => '3',
            'description' => '',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('paths')->insert([
            'dungeon_id' => '5',
            'name' => 'Ferrah',
            'tag' => '1',
            'description' => 'The threat of a Flame Legion superweapon is too dangerous to ignore',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('paths')->insert([
            'dungeon_id' => '5',
            'name' => 'Magg',
            'tag' => '2',
            'description' => 'Gaherons return would mean an inspired Flame Legion. He must stay dead',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('paths')->insert([
            'dungeon_id' => '5',
            'name' => 'Rhiannon',
            'tag' => '3',
            'description' => 'Killing the new tribune will disrupt the Flame Legion',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('paths')->insert([
            'dungeon_id' => '6',
            'name' => 'The butcher',
            'tag' => '1',
            'description' => 'He must be stopped before he launches his attack',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('paths')->insert([
            'dungeon_id' => '6',
            'name' => 'The plunderer',
            'tag' => '2',
            'description' => 'Your culture will not be erased so casually',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('paths')->insert([
            'dungeon_id' => '6',
            'name' => 'The zealot',
            'tag' => '3',
            'description' => 'Jormags madness will not take over this Sanctuary',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('paths')->insert([
            'dungeon_id' => '7',
            'name' => '',
            'tag' => '1',
            'description' => 'Submarine',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('paths')->insert([
            'dungeon_id' => '7',
            'name' => '',
            'tag' => '2',
            'description' => 'Experimental transporter',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('paths')->insert([
            'dungeon_id' => '7',
            'name' => '',
            'tag' => '3',
            'description' => 'Lets leave by the front door',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('paths')->insert([
            'dungeon_id' => '8',
            'name' => '',
            'tag' => '1',
            'description' => 'Forgotten',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('paths')->insert([
            'dungeon_id' => '8',
            'name' => '',
            'tag' => '2',
            'description' => 'Jotun',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('paths')->insert([
            'dungeon_id' => '8',
            'name' => '',
            'tag' => '3',
            'description' => 'Mursaat',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('paths')->insert([
            'dungeon_id' => '8',
            'name' => '',
            'tag' => '4',
            'description' => 'Seer',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);


    }
}

class RoleTableSeeder extends Seeder {

    public function run()
    {
        DB::table('roles')->insert([
            'role' => 'Super Admin',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('roles')->insert([
            'role' => 'Admin',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('roles')->insert([
            'role' => 'User',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

    }
}

class OrderTableSeeder extends Seeder {

    public function run()
    {
        DB::table('orders')->insert([
            'order' => 'None',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('orders')->insert([
            'order' => 'Durmand Priory',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('orders')->insert([
            'order' => 'Vigil',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('orders')->insert([
            'order' => 'Order of Whispers',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

    }
}
class CommentTableSeeder extends Seeder {
    public function run()
    {
        DB::table('comments')->insert([
            'user_id' => '2',
            'creator_id' => '1',
            'comment' => 'Has been inactive for a long time!',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
    }
}

class GuildTableSeeder extends Seeder {
    public function run()
    {
        DB::table('guilds')->insert([
            'name' => 'The Fairytale Knights',
            'tag' => 'TALE'
        ]);
    }
}


class CharacterTableSeeder extends Seeder {

    public function run()
    {
        DB::table('characters')->insert([
            'user_id' => '1',
            'name' => 'Jack Oakfall',
            'level' => '80',
            'profession_id' =>'2',
            'race_id' =>'5',
            'personal_story'=>'1',
            'world_completion'=>'1',
            'order_id' =>'3',
            'fractal_level'=>'5',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('characters')->insert([
            'user_id' => '1',
            'name' => 'Daugther Of Oakfall',
            'level' => '80',
            'profession_id' =>'1',
            'race_id' =>'4',
            'personal_story'=>'0',
            'world_completion'=>'0',
            'order_id' =>'2',
            'fractal_level'=>'0',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('characters')->insert([
            'user_id' => '1',
            'name' => 'Heir Of Oakfall',
            'level' => '31',
            'profession_id' =>'5',
            'race_id' =>'3',
            'personal_story'=>'0',
            'world_completion'=>'0',
            'order_id' =>'4',
            'fractal_level'=>'0',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('characters')->insert([
            'user_id' => '1',
            'name' => 'Anger Of Oakfall',
            'level' => '80',
            'profession_id' =>'6',
            'race_id' =>'3',
            'personal_story'=>'0',
            'world_completion'=>'0',
            'order_id' =>'1',
            'fractal_level'=>'0',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('characters')->insert([
            'user_id' => '1',
            'name' => 'Root Of Oakfall',
            'level' => '21',
            'profession_id' =>'4',
            'race_id' =>'5',
            'personal_story'=>'0',
            'world_completion'=>'0',
            'order_id' =>'1',
            'fractal_level'=>'0',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

    }
}

class RaceTableSeeder extends Seeder {

    public function run()
    {
        DB::table('races')->insert([
            'race' => 'Asura',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('races')->insert([
            'race' => 'Charr',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('races')->insert([
            'race' => 'Human',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('races')->insert([
            'race' => 'Norn',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('races')->insert([
            'race' => 'Sylvari',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
    }
}

class ProfessionTableSeeder extends Seeder {

    public function run()
    {
        DB::table('professions')->insert([
            'profession' => 'Guardian',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('professions')->insert([
            'profession' => 'Warrior',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('professions')->insert([
            'profession' => 'Engineer',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('professions')->insert([
            'profession' => 'Ranger',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('professions')->insert([
            'profession' => 'Thief',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('professions')->insert([
            'profession' => 'Elementalist',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('professions')->insert([
            'profession' => 'Mesmer',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('professions')->insert([
            'profession' => 'Necromancer',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('professions')->insert([
            'profession' => 'Revenant',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);


    }
}

class PageTableSeeder extends Seeder {

    public function run()
    {
        DB::table('pages')->insert([
            'name' => 'home',
            'content' => '<div class="container">
        <div class="row">
            <div class="col-lg-4">
                <img class="pull-left img-responsive img-circle" src="http://guildhalls2.dev/upload/pages/1/1.jpg">
            </div>
            <div class="col-lg-8">
                <div class="row">
                    <h1 class="pull-right header">Knightly</h1>
                </div>
                <div class="row">
                    <p class="header text-justify">We are a guild dedicated in helping players improve their skills in all aspects of the game. Our mission is to train and help newcomers as well as veterans of the game and the Guild Wars franchise.
                    We believe that everyone has something different to bring to the table and we welcome all kinds of people with open arms and predjuice is not in our vocabulary nor in our minds.</p>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-lg-8">
                <div class="row">
                    <h1 class="header">Power of Friendship</h1>
                </div>
                <div class="row">
                    <p class="header text-justify">But when all that is said and done we also strive for ultimate funness! Besides the weekly Guild Bounties, Dungeons Runs and Tequila Kills we have our occasional Fun Nights everyone can join in on.</br></br> Quiz Night, creating new low-level toons running from one end of Tyria to the other and telling stories in the secret places of the Grove is just examples and you can very well be a part of shaping the fun this guild is having!</p>
                </div>

            </div>
            <div class="col-lg-4">
                <img class="pull-right img-responsive img-circle" src="http://guildhalls2.dev/upload/pages/1/2.jpg">
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-lg-4">
                <img class="pull-left img-responsive img-circle" src="http://guildhalls2.dev/upload/pages/1/3.jpg">
            </div>
            <div class="col-lg-8">
                <div class="row">
                    <h1 class="pull-right header">Companionship</h1>
                </div>
                <div class="row">
                    <p class="header text-justify">We believe that even the hardest dungeons and the most bugged instances can be beat by sticking together and achknowledging the challenge ahead. We do not tolerate rage-quitters, trolls, exploiters or other vile creatures walking the face of Tyria. So if you know how to beat your fears and take on Arah p4 naked or is willing to jump into it without knowing anything about it you might be made of the right stuff to be a member of The Fairytale Knights!</p>
                </div>
            </div>
        </div>
    </div>',
            'active' => 1,
            'homepage' => 1,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('pages')->insert([
            'name' => 'schedule',
            'content' => '<div class="container">
        <h1 class="text-center header">Weekly <span class="text-muted">Schedule</span></h1>
        <table class="table table-bordered table-hover table-responsive">
                        <thead>
                          <tr>
                            <th>Time</th>
                            <th>Monday</th>
                            <th>Tuesday</th>
                            <th>Wednesday</th>
                            <th>Thursday</th>
                            <th>Friday</th>
                            <th>Saturday</th>
                            <th>Sunday</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>07:00pm</td>
                            <td></td>
                            <td>sPvP Training</td>
                            <td></td>
                            <td></td>
                            <td>Fun Night</td>
                            <td></td>
                            <td>Dungeon Runs</td>
                          </tr>
                          <tr>
                            <td>08:30pm</td>
                            <td></td>
                            <td></td>
                            <td>Duelling Tournament</td>
                            <td>Jumping Puzzles</td>
                            <td></td>
                            <td>Guild Missions</td>
                            <td></td>
                          </tr>
                        </tbody>
                    </table>

    </div>',
            'active' => 1,
            'homepage' => 0,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
    ]);
        DB::table('pages')->insert([
            'name' => 'leaders',
            'content' => '  <div class="container">
    <div class="row text-center">
        <h1 class="header">Leaders</h1>
    </div>
        <div class="row text-center">
            <div class="col-lg-4">
                <h2 class="header">JoinerJack</h2>
                <img class="img-responsive img-circle leaders" src="http://guildhalls2.dev/upload/pages/3/Jack.jpg">
            </div>
            <div class="col-lg-4">
                <h2 class="header">Dybt</h2>
                <img class="img-responsive img-circle leaders" src="http://guildhalls2.dev/upload/pages/3/Dybt.jpg">
            </div>
            <div class="col-lg-4">
                <h2 class="header">TopazTaylor</h2>
                <img class="img-responsive img-circle leaders" src="http://guildhalls2.dev/upload/pages/3/Topaz.jpg">
            </div>
        </div>
    </div>',
            'active' => 1,
            'homepage' => 0,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
    }
}

class WorldBossTableSeeder extends Seeder {

    public function run()
    {
        DB::table('worldBosses')->insert([
            'name' => 'Admiral Taidha Covington',
            'zone' => 'Bloodtide Coast',
            'area' => 'Laughing Gull Island',
            'waypoint' => '[&BKgBAAA=]',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBosses')->insert([
            'name' => 'Svanir Shaman Chief',
            'zone' => 'Wayfarer Foothills',
            'area' => "Hunter's Lake",
            'waypoint' => '[&BMIDAAA=]',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBosses')->insert([
            'name' => 'Megadestroyer',
            'zone' => 'Mount Maelstrom',
            'area' => "Maelstrom's Bile",
            'waypoint' => '[&BM0CAAA=]',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBosses')->insert([
            'name' => 'Fire Elemental',
            'zone' => 'Metrica Province',
            'area' => 'Thaumanova Reactor',
            'waypoint' => '[&BEcAAAA=]',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
    }
}

class WorldBossTimesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 1,
            'hour' => '00',
            'minute' => '00',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 1,
            'hour' => '03',
            'minute' => '00',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 1,
            'hour' => '06',
            'minute' => '00',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 1,
            'hour' => '09',
            'minute' => '00',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 1,
            'hour' => '12',
            'minute' => '00',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 1,
            'hour' => '15',
            'minute' => '00',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 1,
            'hour' => '18',
            'minute' => '00',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 1,
            'hour' => '21',
            'minute' => '00',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 1,
            'hour' => '15',
            'minute' => '00',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 1,
            'hour' => '15',
            'minute' => '00',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 1,
            'hour' => '18',
            'minute' => '00',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 1,
            'hour' => '21',
            'minute' => '00',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 2,
            'hour' => '00',
            'minute' => '15',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 2,
            'hour' => '02',
            'minute' => '15',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 2,
            'hour' => '04',
            'minute' => '15',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 2,
            'hour' => '06',
            'minute' => '15',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 2,
            'hour' => '08',
            'minute' => '15',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 2,
            'hour' => '10',
            'minute' => '15',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 2,
            'hour' => '12',
            'minute' => '15',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 2,
            'hour' => '14',
            'minute' => '15',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 2,
            'hour' => '16',
            'minute' => '15',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 2,
            'hour' => '18',
            'minute' => '15',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 2,
            'hour' => '20',
            'minute' => '15',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 2,
            'hour' => '22',
            'minute' => '15',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 3,
            'hour' => '00',
            'minute' => '30',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 3,
            'hour' => '03',
            'minute' => '30',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 3,
            'hour' => '06',
            'minute' => '30',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 3,
            'hour' => '09',
            'minute' => '30',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 3,
            'hour' => '12',
            'minute' => '30',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 3,
            'hour' => '15',
            'minute' => '30',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 3,
            'hour' => '18',
            'minute' => '30',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 3,
            'hour' => '21',
            'minute' => '30',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 4,
            'hour' => '00',
            'minute' => '45',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 4,
            'hour' => '02',
            'minute' => '45',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 4,
            'hour' => '04',
            'minute' => '45',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 4,
            'hour' => '06',
            'minute' => '45',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 4,
            'hour' => '08',
            'minute' => '45',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 4,
            'hour' => '10',
            'minute' => '45',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 4,
            'hour' => '12',
            'minute' => '45',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 4,
            'hour' => '14',
            'minute' => '45',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 4,
            'hour' => '16',
            'minute' => '45',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 4,
            'hour' => '18',
            'minute' => '45',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 4,
            'hour' => '20',
            'minute' => '45',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        DB::table('worldBossTimes')->insert([
            'worldBoss_id' => 4,
            'hour' => '22',
            'minute' => '45',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
    }
}



