<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(array('prefix' => 'admin', 'before' => 'auth|active'), function()
{

    Route::get('/',                      array('as' => 'admin',            'uses' => 'AdminController@showAdminPanel'));
    Route::get('style',                  array('as' => 'style',            'uses' => 'StyleController@showStyleSection'));

    Route::get('settings',               array('as' => 'settings',         'uses' => 'SettingsController@getSettingsSection'));
    Route::post('settings',              array('as' => 'settings',         'uses' => 'SettingsController@postSettingsSection'));


    Route::get ('users',                 array('as' => 'users',            'uses' => 'UsersController@showUsers'));
    Route::get ('user/{id}',             array('as' => 'user',             'uses' => 'UserController@showUser'));
    Route::get ('createUser',            array('as' => 'createUser',       'uses' => 'UserController@getCreateUser'));
    Route::post('createUser',            array('as' => 'createUser',       'uses' => 'UserController@postCreateUser'));
    Route::get ('user/{id}/edit',        array(                            'uses' => 'UserController@getEditUser'));
    Route::post('user/{id}/edit',        array('as' => 'editUser',         'uses' => 'UserController@postEditUser'));

    Route::post('user/{id}', array('uses' => 'CommentController@postCreateComment'));


    Route::get ('createCharacter',       array('as' => 'createCharacter',  'uses' => 'CharacterController@getCreateCharacter'));
    Route::post('createCharacter',       array('as' => 'createCharacter',  'uses' => 'CharacterController@postCreateCharacter'));
    Route::get ('character/{id}',        array('as' => 'character',        'uses' => 'CharacterController@ShowCharacter'));
    Route::get ('character/{id}/edit',   array(                            'uses' => 'CharacterController@getEditCharacter'));
    Route::post('character/{id}/edit',   array('as' => 'editCharacter',    'uses' => 'CharacterController@postEditCharacter'));
    Route::get ('character/{id}/delete', array('as' => 'deleteCharacter',  'uses' => 'CharacterController@getDeleteCharacter'));
    Route::post('character/{id}/delete', array('as' => 'deleteCharacter',  'uses' => 'CharacterController@postDeleteCharacter'));

});

Route::get ('/', array('as' => 'home', 'uses' => 'HomeController@showHome'));

Route::get ('page/{id}', array('as' => 'page', 'uses' => 'PageController@showPage'));

Route::get ('register', array('as' => 'register', 'uses' => 'RegisterController@showRegister'));

Route::post('register', 'RegisterController@showResponse');

Route::get ('login', array('as' => 'login', 'uses' => 'LoginController@showLogin'));

Route::post('login', 'LoginController@showAfterLogin');

Route::get ('logout', array('as' => 'logout', 'uses' => 'LogoutController@showLogout'));

Route::get ('reset',  array('as' => 'reset',  'uses' => 'ResetController@showReset'));

Route::get ('dungeonMaster', array('as' => 'dungeonMaster', 'uses' => 'DungeonMasterController@getDungeonMaster'));

Route::post ('dungeonMaster', array('as' => 'dungeonMaster', 'uses' => 'DungeonMasterController@postDungeonMaster'));

Route::get('worldBossTimer', array('as' => 'worldBossTimer', 'uses' => 'WorldBossTimerController@getWorldBossTimer'));

Route::post('worldBossTimer', array('as' => 'worldBossTimer', 'uses' => 'WorldBossTimerController@postWorldBossTimer'));





/*Route::get('/admin', 'AdminController@showAdminPanel');*/
