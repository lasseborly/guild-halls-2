<?php

class RegisterController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    |
    |
    |
    |
    |
    |
    */
    public function showRegister()
    {
        return View::make('register');
    }

    public function showResponse()
    {
        $email = Input::get('email');
        $emailrepeat = Input::get('emailrepeat');
        $password = Input::get('password');
        $passwordrepeat = Input::get('passwordrepeat');
        $displayname = Input::get('displayname');

        $errors[] = null;
        $error = false;
        if(!preg_match ('/\.\d{4}/', $displayname) AND $displayname !== '')
        {
            $errors[] = Lang::get('errors.notadisplayname');
            $error = true;
        }
        if($displayname === '')
        {
            $errors[] = Lang::get('errors.missingdisplayname');
            $error = true;
        }
        if($email === '')
        {
            $errors[] = Lang::get('errors.missingemail');
            $error = true;
        }
        if($password === '')
        {
            $errors[] = Lang::get('errors.missingpassword');
            $error = true;
        }
        if($email !== $emailrepeat AND $email !== '' AND $emailrepeat !== '')
        {
            $errors[] = Lang::get('errors.notsameemail');
            $error = true;
        }
        if($password !== $passwordrepeat AND $password !== '' AND $passwordrepeat !== '')
        {
            $errors[] = Lang::get('errors.notsamepassword');
            $error = true;
        }
        if (strpos($email,'@') === false AND $email !== '' AND $emailrepeat !== '') {
            $errors[] = Lang::get('errors.notanemail');
            $error = true;
        }
        if($error === false)
        {
            $user = new User;
            $user -> email = $email;
            $user -> displayname = $displayname;
            $user -> password = Hash::make($password);
            $user ->role_id = 3;
            $user ->rank_id = 7;
            $user -> save();
            return View::make('login');
        }
        elseif($error === true)
        {
            return Redirect::to('register') -> withErrors($errors);
        }





    }

}