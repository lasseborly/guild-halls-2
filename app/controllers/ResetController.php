<?php

class ResetController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | ForgotLogin Controller
    |--------------------------------------------------------------------------
    |
    |
    |
    |
    |
    |
    |
    */
    public function showReset()
    {
        return View::make('reset');
    }

    public function showAfterReset()
    {

    }

}