<?php

class WorldBossTimerController extends Controller {

    public function getWorldBossTimer() {

        $worldBossTimes = WorldBossTime::orderBy('hour')->orderBy('minute')->get();



        return View::make('worldBossTimer')->with(array('worldBossTimes' => $worldBossTimes));
    }

    public function postWorldBossTimer() {

    }

}
