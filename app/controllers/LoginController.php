<?php

class LoginController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    |
    |
    |
    |
    |
    |
    */
    public function showLogin()
    {
        return View::make('login');
    }

    public function showAfterLogin()
    {
        $credentials = [
            'email' => Input::get('email'),
            'password' => Input::get('password')
        ];
        $remember = Input::get('remember');

        Log::error($remember);



        if(Auth::attempt($credentials)) {
            Log::error($credentials);
            return Redirect::intended('admin');
        }

        return Redirect::to('login');
    }

}