<?php

class UserController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | User Controller
    |--------------------------------------------------------------------------
    |
    |
    |
    |
    |
    |
    |
    */

    public function showUser($id)
    {
        $user = User::find($id);
        return View::make('admin.user')->with(array('user' => $user));
    }

    public function getCreateUser()
    {
        return View::make('admin.createUser');
    }

    public function postCreateUser()
    {
        $email = Input::get('email');
        $password = Input::get('password');
        $displayname = Input::get('displayname');
        $role = Input::get('role');
        $rank = Input::get('rank');

        $user = new User;
        $user -> email = $email;
        $user -> displayname = $displayname;
        $user -> password = Hash::make($password);
        $user -> role_id = $role;
        $user -> rank_id = $rank;
        $user -> save();

        return View::make('admin.createUser')->with('displayname', $displayname);
    }

    public function getEditUser($id) {
        $user = User::find($id);
        return View::make('admin.user.edit')->with(array('user' => $user));
    }

    public function postEditUser($id) {

        $user =  User::find($id);

        $email = Input::get('email');
        $password = Input::get('password');
        $displayname = Input::get('displayname');
        if(Auth::user()->role->role == 'User'){
            $role = Auth::user()->role->id;
            $rank = Auth::user()->rank->id;
        }
        else
        {
            $role = Input::get('role');
            $rank = Input::get('rank');

        }
        if($role == 0){
            $role = $user->role;
        }
        if($rank == 0){
            $rank = $user->rank;
        }
        $achievement_points = Input::get('achievement_points');
        $active = Input::get('active');
        if($active === null){
            $active = 0;
        }


        $input = array('image' => Input::file('image'));
        $rules = array('photo' => 'mimes:jpeg|png');
        $validator = Validator::make($input,$rules);

        if (Input::hasFile('image'))
        {
            if($validator->passes()) {
                Input::file('image')->move('upload/user_images/'.$displayname.'/', $displayname.'.jpg');
            }

        }


        $user -> email = $email;
        $user -> displayname = $displayname;
        if($password != ''){
            $user -> password = Hash::make($password);
        }
        $user -> role_id = $role;
        $user -> rank_id = $rank;
        $user -> achievement_points = $achievement_points;
        $user -> active = $active;
        $user -> save();

        return View::make('admin.user')->with(array('user' => $user));
    }



}