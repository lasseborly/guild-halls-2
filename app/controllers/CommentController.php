<?php

class CommentController extends Controller {

    public function postCreateComment($id) {
        $commenttext = Input::get('comment');
        $user = User::find($id);

        $comment = new Comment();
        $comment -> user_id = $id;
        $comment -> creator_id = Auth::user()->id;
        $comment -> comment = $commenttext;
        $comment -> save();

        return View::make('admin.user')->with(array('user' => $user));

    }

}
