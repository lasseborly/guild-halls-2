<?php

class SettingsController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Settings Controller
    |--------------------------------------------------------------------------
    |
    |
    |
    |
    |
    |
    |
    */
    public function getSettingsSection() {
        return View::make('admin.settings');
    }

    public function postSettingsSection() {

        $name = Input::get('guildname');
        $tag  = Input::get('guildtag');


        $guild = Guild::find(1);
        $guild -> name = $name;
        $guild -> tag  = $tag;
        $guild -> save();


        return View::make('admin.settings')->with('message', 'Guild is named '.$name.' with tag ['.$tag.']');
    }



}