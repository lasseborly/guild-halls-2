<?php

class CharacterController extends Controller {

    public function showCharacter($id)
    {
        $character = Character::find($id);
        return View::make('admin.character')->with(array('character' => $character));
    }

    public function getCreateCharacter()
    {
        return View::make('admin.createCharacter');
    }

    public function postCreateCharacter()
    {
        $name            = Input::get('name');
        $level           = Input::get('level');
        $sex             = Input::get('sex');
        $race            = Input::get('race');
        $profession      = Input::get('profession');
        $order           = Input::get('order');
        if($order === null){
            $order = 0;
        }
        $worldcompletion = Input::get('worldcompletion');
        if($worldcompletion === null){
            $worldcompletion = 0;
        }
        $personalstory   = Input::get('personalstory');
        if($personalstory === null){
            $personalstory = 0;
        }
        $fractallevel    = Input::get('fractallevel');

        $image = null;
        if (Input::hasFile('image'))
        {
            $image = Input::file('image');
        }


        $character = new Character();
        $character -> user_id          = Session::get('relevantuser')->id;
        $character -> name             = $name;
        $character -> level            = $level;
        $character -> profession_id    = $profession;
        $character -> race_id          = $race;
        $character -> sex              = $sex;
        $character -> personal_story   = $personalstory;
        $character -> world_completion = $worldcompletion;
        $character -> fractal_level    = $fractallevel;
        $character -> order_id         = $order;
        $character -> save();

        return View::make('admin.character')->with(array('character' => $character));
    }

    public function getEditCharacter($id) {
        $character = Character::find($id);
        return View::make('admin.character.edit')->with(array('character' => $character));
    }

    public function postEditCharacter($id) {

        $character = Character::find($id);

        $name            = Input::get('name');
        $level           = Input::get('level');
        $sex             = Input::get('sex');
        $race            = Input::get('race');
        $profession      = Input::get('profession');
        $order           = Input::get('order');
        if($order === null){
            $order = 0;
        }
        $worldcompletion = Input::get('worldcompletion');
        if($worldcompletion === null){
            $worldcompletion = 0;
        }
        $personalstory   = Input::get('personalstory');
        if($personalstory === null){
            $personalstory = 0;
        }
        $fractallevel    = Input::get('fractallevel');

        $input = array('image' => Input::file('image'));
        $rules = array('photo' => 'mimes:jpeg|png');
        $validator = Validator::make($input,$rules);

        if (Input::hasFile('image'))
        {
            if($validator->passes()) {
                Input::file('image')->move('upload/user_images/'.$character->user->displayname.'/characters/', $character->user->id.$character->id.'.jpg');
            }
        }

        $character -> user_id          = $character->user->id;
        $character -> name             = $name;
        $character -> level            = $level;
        $character -> profession_id    = $profession;
        $character -> race_id          = $race;
        $character -> sex              = $sex;
        $character -> personal_story   = $personalstory;
        $character -> world_completion = $worldcompletion;
        $character -> fractal_level    = $fractallevel;
        $character -> order_id         = $order;
        $character -> save();

        return View::make('admin.character')->with(array('character' => $character));
    }

    public function getDeleteCharacter($id) {
        $character = Character::find($id);
        return View::make('admin.character.delete')->with(array('character' => $character));
    }

    public function postDeleteCharacter($id) {
        $user = Character::find($id)->user;
        Character::find($id)->delete();

        return View::make('admin.user')->with(array('user' => $user));
    }

}