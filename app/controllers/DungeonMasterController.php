<?php

class DungeonMasterController extends Controller {

    public function getDungeonMaster() {

        $dungeons = Dungeon::all();

        return View::make('dungeonMaster')->with(array('dungeons' => $dungeons));
    }

    public function postDungeonMater() {

    }

}
