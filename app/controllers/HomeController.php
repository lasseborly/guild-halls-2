<?php

class HomeController extends Controller {

	public function showHome()
	{
        $home = Page::where('homepage', '=', 1)->get();
        return View::make('page')->with('page', $home[0]);
	}

}
