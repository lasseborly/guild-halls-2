<?php

class PageController extends Controller {

    public function showPage($id)
    {
        $page = Page::find($id);

        return View::make('page')->with('page', $page);
    }

}
