<?php

class AdminController extends Controller {

    public function showAdminPanel()
    {
        //Reddit
        $string = file_get_contents('http://www.reddit.com/r/fairytaleknights/new.json?sort=new');
        $result = json_decode($string);
        $allSubredditPosts = $result->data->children;
        $subredditPosts = array();

        for ($x = 0; $x <= 2; $x++) {
            if(isset($allSubredditPosts[$x])){
                $subredditPosts[] = $allSubredditPosts[$x];
            }
        }

        //Admin Comments
        $allComments = Comment::orderBy('created_at', 'DESC')->get();
        $comments = array();

        $counter = 0;
        $commentsAmount = 3;
        foreach($allComments as $comment){
            if($counter == $commentsAmount){
                break;
            }
            elseif(Auth::user()->id != $comment->user->id){
                $comments[] = $comment;
                $counter++;
            }
        }

        //Statistics
        $totalCharacters = Character::all()->count();

            //Professions
        $professions[] = array();
        $professions['guardians']     = Character::where('profession_id', '=', 1)->count();
        $professions['warriors']      = Character::where('profession_id', '=', 2)->count();
        $professions['engineers']     = Character::where('profession_id', '=', 3)->count();
        $professions['rangers']       = Character::where('profession_id', '=', 4)->count();
        $professions['thiefs']        = Character::where('profession_id', '=', 5)->count();
        $professions['elementalists'] = Character::where('profession_id', '=', 6)->count();
        $professions['mesmers']       = Character::where('profession_id', '=', 7)->count();
        $professions['necromancers']  = Character::where('profession_id', '=', 8)->count();
        $professions['revenants']     = Character::where('profession_id', '=', 9)->count();

            //Races
        $races = array();
        $races['asuras']   = Character::where('race_id', '=', 1)->count();
        $races['charrs']   = Character::where('race_id', '=', 2)->count();
        $races['humans']   = Character::where('race_id', '=', 3)->count();
        $races['norns']    = Character::where('race_id', '=', 4)->count();
        $races['sylvaris'] = Character::where('race_id', '=', 5)->count();

            //Orders
        $orders[] = array();
        $orders['durmonds']  = Character::where('order_id', '=', 2)->count();
        $orders['vigils']    = Character::where('order_id', '=', 3)->count();
        $orders['whispers']  = Character::where('order_id', '=', 4)->count();


        return View::make('admin.admin')->with(array("subredditPosts" => $subredditPosts, "comments" => $comments, "professions" => $professions, "races" => $races, "orders" => $orders));
    }



}