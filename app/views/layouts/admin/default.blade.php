@extends('layouts.admin.master')

@section('body')
    <div id="wrapper">
                <!-- Navigation -->
                <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        @if(isset(Guild::find(1)->name))
                        <a class="navbar-brand" href="/">{{Guild::find(1)->name}}</a>
                        @else
                        <a class="navbar-brand" href="/">Guild Name</a>
                        @endif
                    </div>
                    <!-- /.navbar-header -->

                    <ul class="nav navbar-top-links navbar-right">
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                {{Auth::user()->displayname}} <i class="fa fa-caret-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="{{ URL::to('/admin/user/'.Auth::user()->id) }}"><i class="fa fa-user fa-fw"></i> {{Lang::get('menu.profile');}}</a>
                                </li>
                                <li class="divider"></li>
                                <li><a href="{{ URL::route('logout') }}"><i class="fa fa-sign-out fa-fw"></i> {{Lang::get('menu.logOut');}}</a>
                                </li>
                            </ul>
                            <!-- /.dropdown-user -->
                        </li>
                        <!-- /.dropdown -->
                    </ul>
                    <!-- /.navbar-top-links -->

                    <div class="navbar-default sidebar" role="navigation">
                        <div class="sidebar-nav navbar-collapse">
                            <ul class="nav" id="side-menu">
                                <li>
                                    <a href="/admin"><i class="fa fa-pie-chart"></i> {{Lang::get('menu.guildView');}}</a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-users"></i> {{Lang::get('menu.guildMembers');}}<span class="fa arrow"></span></a>
                                    <ul class="nav nav-second-level">
                                        <li>
                                            <a href="{{ URL::route('users') }}"><i class="fa fa-bars"></i> {{Lang::get('menu.allGuildMembers');}}</a>
                                        </li>
                                        <li>
                                            <a href="{{ URL::route('createUser') }}"><i class="fa fa-plus"></i> {{Lang::get('menu.addNew');}}</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-second-level -->
                                </li>
                                <li>
                                    <a href="tables.html"><i class="fa fa-heart"></i> {{Lang::get('menu.tools');}}<span class="fa arrow"></span></a>
                                    <ul class="nav nav-second-level">
                                        <li>
                                            <a href="{{URL::route('dungeonMaster')}}"><i class="fa fa-key"></i> {{Lang::get('menu.dungeonMaster');}}</a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-clock-o"></i> {{Lang::get('menu.worldBossTimer');}}</a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-calendar"></i> {{Lang::get('menu.eventPlanner');}}</a>
                                        </li>
                                    </ul>

                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-file fa-fw"></i> {{Lang::get('menu.pages');}}<span class="fa arrow"></span></a>
                                    <ul class="nav nav-second-level">

                                        <li>
                                            <a href="#"><i class="fa fa-bars"></i> {{Lang::get('menu.allPages');}}</a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-plus"></i> {{Lang::get('menu.addNew');}}</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-second-level -->
                                </li>
                                 <!-- <li>
                                    <a href="{{ URL::route('style');  }}"><i class="fa fa-paint-brush"></i> {{Lang::get('menu.style');}}</a>
                                 </li> -->
                                 <li>
                                    <a href="{{URL::route('settings')}}"><i class="fa fa-gear fa-fw"></i> {{Lang::get('menu.settings');}}</a>
                                 </li>
                            </ul>
                        </div>
                        <!-- /.sidebar-collapse -->
                    </div>
                    <!-- /.navbar-static-side -->
                </nav>

                <!-- Page Content -->
                <div id="page-wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="content">
                                @yield('content')
                            </div>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /#page-wrapper -->

            </div>
            <!-- /#wrapper -->
        </div>
@stop