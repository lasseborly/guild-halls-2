<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('title')</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ URL::asset('_css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{ URL::asset('_css/plugins/metisMenu/metisMenu.min.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ URL::asset('_css/sb-admin-2.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('_css/adminmain.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('_css/plugins/dataTables.bootstrap.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- jquery JavaScripts -->
    <script src="{{URL::asset('_js/jquery-1.11.0.js')}}"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{URL::asset('_js/plugins/metisMenu/metisMenu.min.js')}}"></script>
    <!--Bootstrap JavaScripts -->
    <script src="{{URL::asset('_js/bootstrap.min.js')}}"></script>
    <!--Custom JavaScripts -->
    <script src="{{URL::asset('_js/sb-admin-2.js')}}"></script>
    <script src="{{URL::asset('_js/plugins/dataTables/jquery.dataTables.js')}}"></script>
    <script src="{{URL::asset('_js/plugins/dataTables/dataTables.bootstrap.js')}}"></script>



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="{{URL::asset('_js/respond.min.js')}}"></script>
        <script src="{{URL::asset('_js/html5shiv.js')}}"></script>
    <![endif]-->

</head>

<body>

@yield('body')

</body>