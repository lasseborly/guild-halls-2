@extends('layouts.master')
@section('body')
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{URL::route('home')}}">{{Guild::find(1)->tag}}</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="collapse">
        <ul class="nav navbar-nav">
        @foreach( Page::all() as $page)
            @if($page->homepage != 1)
            <li><a href="{{URL::to('page/'.$page->id)}}">{{$page->name}}</a></li>
            @endif
        @endforeach
            <li><a href="{{URL::route('dungeonMaster')}}">Dungeon Master</a></li>
            <li><a href="{{URL::route('worldBossTimer')}}">World Boss Timer</a></li>

        </ul>
      </div><!-- /.navbar-collapse -->
  </div>
</nav>

@yield('content')

<nav class="navbar navbar-default navbar-fixed-bottom" role="navigation">
    <div class="container-fluid">
        <a class="pull-right login-link" href="{{URL::Route('admin')}}">Members</a>
    </div>
</nav>
@stop