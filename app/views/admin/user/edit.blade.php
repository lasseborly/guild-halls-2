@extends('layouts.admin.default')
@section('title','Edit '.$user->displayname)
@section('content')
<div class="row">
    <div class="col-lg-3">
    <h2>{{Lang::get('editUser.edit')}} {{$user->displayname}}</h2>
        <div class="panel panel-default margin-top">
            <div class="panel-body">
               {{ Form::open(array('url' => 'admin/user/'.$user->id.'/edit', 'files' => true)) }}
                   <div class='form-group'>
                       {{ Form::label('displayname', Lang::get('editUser.displayName')); }}
                       {{ Form::text('displayname', $user->displayname, ['placeholder' => Lang::get('register.displayname'), 'class' => 'form-control']) }}
                   </div>
                   <div class='form-group'>
                       {{ Form::label('email', Lang::get('editUser.email')); }}
                       {{ Form::text('email', $user->email, ['placeholder' => Lang::get('register.email'), 'class' => 'form-control']) }}
                   </div>
                   <div class='form-group'>
                       {{ Form::label('password', Lang::get('editUser.password')); }}
                       {{ Form::password('password', ['placeholder' => Lang::get('register.password'), 'class' => 'form-control']) }}
                   </div>
                   @if(Auth::user()->role->role == 'Admin' OR Auth::user()->role->role == 'Super Admin')
                   <div class='form-group'>
                        {{ Form::label('role', Lang::get('editUser.role')); }}
                        </br>
                        @if(isset($user->role->id))
                        {{ Form::select('role', DataHelper::getRoles(), $user->role->id)}}
                        @else
                        {{ Form::select('role', DataHelper::getRoles())}}
                        @endif
                   </div>
                   <hr>
                   <div class='form-group'>
                        {{ Form::label('rank', Lang::get('editUser.rank')); }}
                        </br>
                        @if(isset($user->rank->id))
                        {{ Form::select('rank', DataHelper::getRanks(), $user->rank->id); }}
                        @else
                        {{ Form::select('rank', DataHelper::getRanks()); }}
                        @endif
                   </div>
                   @endif
                   <div class='form-group'>
                        {{ Form::label('image', Lang::get('editUser.achievementPoints')); }}
                        </br>
                        @if(isset($user->achievement_points))
                        {{ Form::number('achievement_points', $user->achievement_points); }}
                        @else
                        {{ Form::number('achievement_points'); }}
                        @endif
                   </div>
                   <div class='form-group'>
                        {{ Form::label('image', Lang::get('editUser.uploadImage')); }}
                        {{ Form::file('image'); }}
                   </div>
                   @if(Auth::user()->id != $user->id)
                       <div class='form-group'>
                            {{ Form::checkbox('active', '1', $user->active); }}
                            {{ Form::label('active', Lang::get('editUser.active')); }}
                       </div>
                   @endif
                   <div class='form-group'>
                       {{ Form::submit(Lang::get('editUser.editUser'), ['class' => 'btn btn-lg btn-block custom-color']) }}
                   </div>
                   {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
    @if (isset($displayname))
        <div class='alert alert-success'>{{ $displayname }} {{Lang::get('editUser.hasBeenSuccessfullyEdited')}}</div>
        @endif
    </div>
</div>
@stop