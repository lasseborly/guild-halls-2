@extends('layouts.admin.default')
@section('title',$user->displayname)
@section('content')
@if(Session::has('relevantuser'))
    {{ Session::forget('relevantuser') }}
@endif
{{ Session::put('relevantuser', $user); }}
<div class="row">
    <div class="col-lg-6">
        <ol class="breadcrumb breadcrump-bar">
            <li><a href="{{URL::route('users')}}">{{Lang::get('user.users');}}</a></li>
            <li>{{$user->displayname}}</li>
        </ol>
    </div>
</div>
<div class="row user-container">
    <div class="col-lg-6">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6 achievement_points">
                        <img class="img-responsive" src="{{ URL::asset('_images/achievement/Achievement_Points.png') }}">
                        <p>@if(isset($user->achievement_points)){{ $user->achievement_points }}@endif</p>
                    </div>
                    <div class="col-lg-6">
                        <p class="displayname">{{ $user->displayname }}</p>
                    </div>
                </div>
                <hr>
                    <div class="row">
                        <div class="col-lg-5">
                            <img class="img-responsive img-rounded user-image" src="{{URL::asset('upload/user_images/'.$user->displayname.'/'.$user->displayname.'.jpg')}}">
                        </div>
                        <div class="col-lg-6">
                            <div class="panel panel-default user-info-panel pull-right">
                                <div class="panel-body">
                                    <h4>@if(isset($user->rank->rank)){{ $user->rank->rank }}@endif</h4>
                                    <hr>
                                    <h4>{{ $user->role->role }}</h4>
                                    <hr>
                                    <h4><a href="mailto:{{ $user->email }}">{{ $user->email  }}</a></h4>
                                </div>
                            </div>
                       </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                        @if(Auth::user()->id === $user->id OR Auth::user()->role->role == 'Admin' OR Auth::user()->role->role == 'Super Admin')
                            <a href="{{ URL::to('admin/user/'.$user->id.'/edit') }}"><button class="btn btn-lg custom-color pull-right"><i class="fa fa-pencil-square-o"></i> {{Lang::get('user.edit');}}</button></a>
                        @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(Auth::user()->id != $user->id)
        <div class="row">
           <div class="panel panel-default">
             <div class="panel-body">
               <h1>New Comment</h1>
               {{ Form::open(array('url' => 'admin/user/'.$user->id)) }}
               {{ Form::textarea('comment', null, ['placeholder' => 'Comment goes here', 'class' => 'form-control']);}}
               {{ Form::submit('Submit', ['class' => 'btn btn-lg btn-comment custom-color pull-right']);}}
               {{ Form::close() }}
             </div>
           </div>
        </div>
        <div class="row">
            @if(Auth::user()->role->role == 'Admin' OR Auth::user()->role->role == 'Super Admin')
                @if(count($user->comments) > 0)
                <div class="panel panel-default">
                      <div class="panel-body">
                      <h1>Comments</h1>
                            @foreach($user->comments as $comment)
                            <div class="panel panel-default">
                                <div class="panel-heading"><a href="{{ URL::to('admin/user/'.$comment->creatorUser->id) }}">{{$comment->creatorUser->displayname}}</a>  <small class="pull-right">{{$comment->created_at}}</small></div>
                                <div class="panel-body">
                                        {{$comment->comment}}
                                </div>
                            </div>
                            @endforeach
                      </div>
                </div>
                @endif
            @endif
        </div>
        @endif
    </div>
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-body">
            <h2 class="character-header">{{Lang::get('user.characters');}}</h2>
                    @if(count($user->characters) > 0)
                        @foreach($user->characters as $character)
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row characters-panel">
                                    <div class="col-lg-3">
                                        <a href="{{ URL::to('admin/character/'.$character->id) }}"><img class="img-rounded img-responsive character-image-small" src="{{ URL::asset('upload/user_images/'.$user->displayname.'/characters/'.$user->id.$character->id.'.jpg') }}"></a>
                                    </div>
                                    <div class="col-lg-6 character-info">
                                        <h4 class="media-heading"><a href="{{ URL::to('admin/character/'.$character->id) }}">{{ $character->name }}</a></h4>
                                        <h5 class="character-level"><b>{{Lang::get('user.level');}}:</b> {{ $character->level }}</h5>
                                        <h5 class="character-race"><b>{{Lang::get('user.race');}}:</b> {{$character->race->race}}</h5>
                                        <h5 class="character-profession"><b>{{Lang::get('user.profession');}}:</b> {{ $character->profession->profession }}</h5>
                                    </div>
                                </div>
                        </div>
            </div>
                        @endforeach
                    @endif
                    @if(Auth::user()->id === $user->id OR Auth::user()->role->role == 'Admin' OR Auth::user()->role->role == 'Super Admin')
                    <a href="{{ URL::route('createCharacter') }}"><button class="btn btn-lg custom-color pull-right"><i class="fa fa-plus"></i> {{Lang::get('user.newCharacter');}}</button></a>
                    @endif
            </div>
        </div>

    </div>

</div>

@stop


