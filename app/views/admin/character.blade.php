@extends('layouts.admin.default')
@section('title', $character->name)
@section('content')

<div class="row">
    <div class="col-lg-4">
        <ol class="breadcrumb breadcrump-bar">
            <li><a href="{{URL::route('users')}}">{{Lang::get('character.users')}}</a></li>
            <li><a href="{{URL::to('admin/user/'.$character->user->id)}}">{{$character->user->displayname}}</a></li>
            <li>{{$character->name}}</li>
        </ol>
    </div>
</div>
<div class="row character-container">
    <div class="col-lg-4">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default character-panel">
                    <div class="panel-body">
                        <div class="row text-center">
                            <div class="col-lg-4">
                                <h4>{{Lang::get('character.level')}}</h4>
                                <hr>
                                <h1 class="character-level">{{ $character->level }}</h1>
                            </div>
                            <div class="col-lg-4">
                            <h4>{{Lang::get('character.race')}}</h4>
                            <hr>
                            @if($character->race->race == 'Sylvari')
                                <img class="img-responsive" src="{{ URL::asset('_images/races/sylvari/Sylvari_tango_icon_200px.png') }}">
                            @elseif($character->race->race == 'Human')
                                <img class="img-responsive" src="{{ URL::asset('_images/races/human/Human_tango_icon_200px.png') }}">
                            @elseif($character->race->race == 'Asura')
                                <img class="img-responsive" src="{{ URL::asset('_images/races/asura/Asura_tango_icon_200px.png') }}">
                            @elseif($character->race->race == 'Norn')
                                <img class="img-responsive" src="{{ URL::asset('_images/races/norn/Norn_tango_icon_200px.png') }}">
                            @elseif($character->race->race == 'Charr')
                                <img class="img-responsive" src="{{ URL::asset('_images/races/charr/Charr_tango_icon_200px.png') }}">
                            @endif
                            </div>
                            <div class="col-lg-4">
                            <h4>{{Lang::get('character.profession')}}</h4>
                            <hr>
                            @if($character->profession->profession == 'Warrior')
                                <img class="img-responsive" src="{{ URL::asset('_images/professions/warrior/Warrior_tango_icon_200px.png') }}">
                            @elseif($character->profession->profession == 'Guardian')
                                <img class="img-responsive" src="{{ URL::asset('_images/professions/guardian/Guardian_tango_icon_200px.png') }}">
                            @elseif($character->profession->profession == 'Necromancer')
                                <img class="img-responsive" src="{{ URL::asset('_images/professions/necromancer/Necromancer_tango_icon_200px.png') }}">
                            @elseif($character->profession->profession == 'Thief')
                                <img class="img-responsive" src="{{ URL::asset('_images/professions/thief/Thief_tango_icon_200px.png') }}">
                            @elseif($character->profession->profession == 'Ranger')
                                <img class="img-responsive" src="{{ URL::asset('_images/professions/ranger/Ranger_tango_icon_200px.png') }}">
                            @elseif($character->profession->profession == 'Elementalist')
                                <img class="img-responsive" src="{{ URL::asset('_images/professions/elementalist/Elementalist_tango_icon_200px.png') }}">
                            @elseif($character->profession->profession == 'Engineer')
                                <img class="img-responsive" src="{{ URL::asset('_images/professions/engineer/Engineer_tango_icon_200px.png') }}">
                            @elseif($character->profession->profession == 'Mesmer')
                                <img class="img-responsive" src="{{ URL::asset('_images/professions/mesmer/Mesmer_tango_icon_200px.png') }}">
                            @endif
                            </div>
                        </div>


                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default character-panel">
                    <div class="panel-body">
                        <div class="row text-center">
                            <div class="col-lg-4">
                                <h4>{{Lang::get('character.worldCompletion')}}</h4>
                                <hr>
                                @if($character->world_completion == 1)
                                <img class="img-responsive" src="{{ URL::asset('_images/world_completion/Map_Explored_100.png') }}">
                                @else
                                <img class="img-responsive" src="{{ URL::asset('_images/world_completion/Map_Explored_25.png') }}">
                                @endif
                            </div>
                            <div class="col-lg-4">
                                <h4>{{Lang::get('character.order')}}</h4></br>
                                <hr>
                                @if($character->order->order == 'Vigil')
                                <img class="img-responsive" src="{{ URL::asset('_images/orders/Vigil_banner.png') }}">
                                @elseif($character->order->order == 'Order of Whispers')
                                <img class="img-responsive" src="{{ URL::asset('_images/orders/Order_of_Whispers_banner.png') }}">
                                @elseif($character->order->order == 'Durmand Priory')
                                <img class="img-responsive" src="{{ URL::asset('_images/orders/Durmand_Priory_banner.png') }}">
                                @else
                                <img class="img-responsive" src="{{ URL::asset('_images/orders/Empty_Durmand_Priory_banner.png') }}">
                                @endif
                            </div>
                            <div class="col-lg-4">
                                <h4>{{Lang::get('character.personalStory')}}</h4>
                                <hr>
                                @if($character->personal_story == 1)
                                <img class="img-responsive" src="{{ URL::asset('_images/achievement/Personal_story_mission_complete.png') }}">
                                @else
                                <img class="img-responsive" src="{{ URL::asset('_images/achievement/Personal_story_mission_incomplete.png') }}">
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 text-center">
        <div class="panel panel-default character-panel">
            <div class="panel-body">
                <h1 class="character-name">{{ $character->name }}</h1>
                <img class="img-rounded img-responsive character-image" src="{{ URL::asset('upload/user_images/'.$character->user->displayname.'/characters/'.$character->user->id.$character->id.'.jpg') }}">
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="panel panel-default crafting-panel">
            <div class="panel-body text-center">
                <div class="row">
                    <div class="col-lg-3">
                        <h4>500</h4>
                        <img class="img-responsive" src="{{ URL::asset('_images/crafting/Armorsmith_tango_icon.png') }}">
                    </div>
                    <div class="col-lg-3">
                        <h4>245</h4>
                        <img class="img-responsive" src="{{ URL::asset('_images/crafting/Artificer_tango_icon.png') }}">
                    </div>
                    <div class="col-lg-3">
                        <h4>230</h4>
                        <img class="img-responsive" src="{{ URL::asset('_images/crafting/Chef_tango_icon.png') }}">
                    </div>
                    <div class="col-lg-3">
                        <h4>110</h4>
                        <img class="img-responsive" src="{{ URL::asset('_images/crafting/Huntsman_tango_icon.png') }}">
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-lg-3">
                        <img class="img-responsive" src="{{ URL::asset('_images/crafting/Jeweler_tango_icon.png') }}">
                        <h4>0</h4>
                    </div>
                    <div class="col-lg-3">
                        <img class="img-responsive" src="{{ URL::asset('_images/crafting/Leatherworker_tango_icon.png') }}">
                        <h4>120</h4>
                    </div>
                    <div class="col-lg-3">
                        <img class="img-responsive" src="{{ URL::asset('_images/crafting/Tailor_tango_icon.png') }}">
                        <h4>400</h4>
                    </div>
                    <div class="col-lg-3">
                        <img class="img-responsive" src="{{ URL::asset('_images/crafting/Weaponsmith_tango_icon.png') }}">
                        <h4>500</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-lg-6 pull-right">
    @if(Auth::user()->id === $character->user->id OR Auth::user()->role->role == 'Admin' OR Auth::user()->role->role == 'Super Admin')
        <a href="{{ URL::to('admin/character/'.$character->id.'/edit') }}"><button class="btn btn-lg custom-color edit-character pull-right"><i class="fa fa-pencil-square-o"></i> {{Lang::get('character.edit')}}</button></a>
        <a href="{{ URL::to('admin/character/'.$character->id.'/delete') }}"><button class="btn btn-lg custom-color pull-right delete-character"><i class="fa fa-times"></i> {{Lang::get('character.delete')}}</button></a>
    @endif
    </div>
</div>

@stop