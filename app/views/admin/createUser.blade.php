@extends('layouts.admin.default')
@section('title','Add new user')
@section('content')
<div class="row">
    <div class="col-lg-3">
    <h2>{{Lang::get('newUser.newUser');}}</h2>
        <div class="panel panel-default margin-top">
            <div class="panel-body">
               {{ Form::open(array('url' => 'admin/createUser')) }}
                   <div class='form-group'>
                       {{ Form::label('displayname', Lang::get('newUser.displayName')); }}
                       {{ Form::text('displayname', null, ['placeholder' => 'Ex. JoinerJack.9201', 'class' => 'form-control']) }}
                   </div>
                   <div class='form-group'>
                       {{ Form::label('email', Lang::get('newUser.email')); }}
                       {{ Form::text('email', null, ['placeholder' => 'Ex. contact@guildhalls2.com', 'class' => 'form-control']) }}
                   </div>
                   <div class='form-group'>
                       {{ Form::label('password', Lang::get('newUser.password')); }}
                       {{ Form::password('password', ['placeholder' => ' Ex. AiGfFF6vBn99', 'class' => 'form-control']) }}
                   </div>
                   <div class='form-group'>
                        {{ Form::label('role', Lang::get('newUser.role')); }}
                        </br>
                        {{ Form::select('role', DataHelper::getRoles()); }}
                   </div>
                   <div class='form-group'>
                        {{ Form::label('rank', Lang::get('newUser.rank')); }}
                        </br>
                        {{ Form::select('rank', DataHelper::getRanks()); }}
                   </div>
                   <div class='form-group'>
                        {{ Form::label('image', Lang::get('newUser.uploadImage')); }}
                        {{ Form::file('image'); }}
                   </div>
                   <div class='form-group'>
                       {{ Form::submit(Lang::get('newUser.createUser'), ['class' => 'btn btn-lg btn-block custom-color']) }}
                   </div>
                   {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
    @if (isset($displayname))
        <div class='alert alert-success'>{{ $displayname }} has been created.</div>
        @endif
    </div>
</div>
@stop