@extends('layouts.admin.default')
@section('title', Guild::find(1)->tag)
@section('content')
<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-default comment-admin-panel">
            <div class="panel-body">
                <h1>Reddit</h1>
                @if(count($subredditPosts) > 0)
                    @foreach($subredditPosts as $post)
                    <div class="panel panel-default">
                        <div class="panel-heading"><a href="{{$post->data->url}}">{{$post->data->title}}</a> <small class="pull-right"><a href="http://www.reddit.com/user/{{$post->data->author}}">{{$post->data->author}}</a> - {{gmdate("d-m-Y", $post->data->created);}}</small></div>
                        <div class="panel-body">
                            <p>{{$post->data->selftext}}</p>
                        </div>
                    </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    <div class="col-lg-6">
    @if(Auth::user()->role->role == 'Admin' or Auth::user()->role->role == 'Super Admin')
        <div class="panel panel-default comment-admin-panel">
            <div class="panel-body">
            <h1>{{Lang::get('comments.recentComments');}}</h1>
                    @if(count($comments) > 0)
                        @foreach($comments as $comment)
                        <div class="panel panel-default">
                            <div class="panel-heading">{{Lang::get('comments.regarding');}} <a href="{{ URL::to('admin/user/'.$comment->user->id) }}">{{$comment->user->displayname}}</a><small class="pull-right"><a href="{{ URL::to('admin/user/'.$comment->creatorUser->id) }}">{{$comment->creatorUser->displayname}}</a> - {{gmdate("d-m-Y", $comment->created_at->timestamp)}}</small></div>
                            <div class="panel-body">
                                <p>{{$comment->comment}}</p>
                            </div>
                        </div>
                        @endforeach
                    @endif
            </div>
        </div>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <div id="race-division"></div>
    </div>
    <div class="col-lg-4">
        <div id="character-division"></div>
    </div>
    <div class="col-lg-4">
        <div id="order-division"></div>
    </div>
</div>

    <script src="{{URL::asset('_js/plugins/morris/morris.js')}}"></script>
    <script src="{{URL::asset('_js/plugins/morris/morris-data.js')}}"></script>
    <script src="{{URL::asset('_js/plugins/morris/raphael.min.js')}}"></script>


<script>

{{
    $total = Character::all()->count();
}}

Morris.Donut({
  element: 'character-division',
  data: [
    {label: "Guardians",     value: {{$professions['guardians']}}     },
    {label: "Warriors",      value: {{$professions['warriors']}}      },
    {label: "Engineers",     value: {{$professions['engineers']}}     },
    {label: "Rangers",       value: {{$professions['rangers']}}       },
    {label: "Thiefs",        value: {{$professions['thiefs']}}        },
    {label: "Elementalists", value: {{$professions['elementalists']}} },
    {label: "Mesmers",       value: {{$professions['mesmers']}}       },
    {label: "Necromancers",  value: {{$professions['necromancers']}}  },
    {label: "Revenants",     value: {{$professions['revenants']}}     },

  ],
  colors: [
    "#BCE8FD",
    "#FFF2A4",
    "#E8BC84",
    "#D2F6BC",
    "#DEC6C9",
    "#F6BEBC",
    "#D09EEA",
    "#BFE6D0",
    "#F68A87",
  ],
    resize: true
});

   Morris.Donut({
     element: 'race-division',
     data: [
       {label: "Asuras",   value: {{$races['asuras']}}   },
       {label: "Charrs",   value: {{$races['charrs']}}   },
       {label: "Humans",   value: {{$races['humans']}}   },
       {label: "Norns",    value: {{$races['norns']}}    },
       {label: "Sylvaris", value: {{$races['sylvaris']}} },
     ],
     colors: [
       "#D1BDF8",
       "#FFBCC3",
       "#FFF2B3",
       "#BADDFF",
       "#B0F3B2",
     ],
     resize: true
   });

   Morris.Donut({
     element: 'order-division',
     data: [
       {label: "Durmond Priory",    value: {{$orders['durmonds']}} },
       {label: "Vigil",             value: {{$orders['vigils']}}   },
       {label: "Order of Whispers", value: {{$orders['whispers']}} },
     ],
     colors: [
       "#aebdd0",
       "#261a1a",
       "#862929",
     ],
     resize: true
   });

</script>

@stop