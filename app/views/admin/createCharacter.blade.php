@extends('layouts.admin.default')
@section('title','Add new character')
@section('content')
<div class="row">
    <div class="col-lg-3">
    <h2>Add new Character</h2>
        <div class="panel panel-default margin-top">
            <div class="panel-body">
               {{ Form::open(array('url' => 'admin/createCharacter', 'files' => true)) }}
                   <div class='form-group'>
                       {{ Form::label('name', 'Name'); }}
                       {{ Form::text('name', null, ['placeholder' => 'Ex. Raistlin Majere', 'class' => 'form-control']) }}
                   </div>
                   <div class='form-group'>
                       {{ Form::label('level', 'Level'); }}
                       {{ Form::number('level', null, ['placeholder' => 'Ex. 80', 'class' => 'form-control']) }}
                   </div>
                   <div class="form-group">
                        {{ Form::radio('sex', '0', true); }}
                        {{ Form::label('sex', 'Female'); }}
                        </br>
                        {{ Form::radio('sex', '1'); }}
                        {{ Form::label('sex', 'Male'); }}
                   </div>
                   <div class='form-group'>
                        {{ Form::label('race', 'Race'); }}
                        </br>
                        {{ Form::select('race', DataHelper::getRaces()); }}
                   </div>
                   <div class='form-group'>
                        {{ Form::label('profession', 'Profession'); }}
                        </br>
                        {{ Form::select('profession', DataHelper::getProfessions()); }}
                   </div>
                   <div class='form-group'>
                        {{ Form::label('order', 'Order'); }}
                        </br>
                        {{ Form::select('order', DataHelper::getOrders()); }}
                   </div>
                   <div class='form-group'>
                        {{ Form::checkbox('worldcompletion', '1'); }}
                        {{ Form::label('worldcompletion', 'World Completion'); }}
                        </br>
                        {{ Form::checkbox('personalstory', '1'); }}
                        {{ Form::label('personalstory', 'Completed Personal Story'); }}
                   </div>
                   <div class='form-group'>
                        {{ Form::label('fractallevel', 'Fractal Level'); }}
                        {{ Form::number('fractallevel', null, ['placeholder' => 'Ex. 21', 'class' => 'form-control fractal-level']) }}
                   </div>
                   <div class='form-group'>
                        {{ Form::label('image', 'Upload image'); }}
                        {{ Form::file('image'); }}
                   </div>
                   <div class='form-group'>
                       {{ Form::submit('Add Character', ['class' => 'btn btn-lg btn-block custom-color']) }}
                   </div>
                   {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
    @if (isset($name))
        <div class='alert alert-success'>{{ $name }} has been created.</div>
        @endif
    </div>
</div>
@stop