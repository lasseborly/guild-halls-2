@extends('layouts.admin.default')
@section('title', 'Settings')
@section('content')
<h2>{{Lang::get('settings.guildSettings')}}</h2>
<div class="row">
    <div class="col-lg-3">
         <div class="panel panel-default comment-admin-panel">
            <div class="panel-body">
                       {{ Form::open(array('url' => 'admin/settings')) }}
                       <div class='form-group'>
                           {{ Form::label('guildname', Lang::get('settings.guildName')); }}
                           {{ Form::text('guildname', null, ['placeholder' => 'Ex. The Fairytale Knights', 'class' => 'form-control']) }}
                       </div>
                       <div class='form-group'>
                           {{ Form::label('guildtag', Lang::get('settings.guildTag')); }}
                           {{ Form::text('guildtag', null, ['placeholder' => 'Ex. TALE', 'class' => 'form-control']) }}
                       </div>
                       <div class='form-group'>
                           {{ Form::submit(Lang::get('settings.submit'), ['class' => 'btn btn-lg custom-color pull-right']) }}
                       </div>
                       {{ Form::close() }}
            </div>
         </div>
    </div>
</div>
@stop