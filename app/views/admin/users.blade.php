@extends('layouts.admin.default')
@section('title', 'Guild Members')
@section('content')
<h2>{{Lang::get('guildMembers.guildMembers');}}</h2>
<div class="table-responsive members-list">
    <table id="users" class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th>{{Lang::get('guildMembers.rank');}}</th>
                <th>{{Lang::get('guildMembers.displayName');}}</th>
                <th>{{Lang::get('guildMembers.achievementPoints');}}</th>
                <th>{{Lang::get('guildMembers.email');}}</th>
                <th>{{Lang::get('guildMembers.role');}}</th>
                <th>{{Lang::get('guildMembers.createdAt');}}</th>
                <th>{{Lang::get('guildMembers.active');}}</th>
            </tr>
        </thead>
        <tbody>
        @if(count($users) > 0)
            @foreach($users as $user)
                @if($user->active == 0)
                <tr class="danger">
                @else
                <tr>
                @endif
                    <td>@if(isset($user->rank->rank)){{ $user->rank->rank }} @endif</td>
                    <td><a href="{{ URL::to('admin/user/'.$user->id) }}">{{ $user->displayname }}</a></td>
                    <td>@if(isset($user->achievement_points)){{ $user->achievement_points }} @endif</td>
                    <td><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
                    <td>@if(isset($user->role->role)){{ $user->role->role }}@endif</td>
                    <td>{{$user->created_at}}</td>
                    <td class="text-center">
                        @if($user->active == 1)
                        <p class="hidden">2</p>
                        <i class="fa fa-check text-success"></i>
                        @else
                        <p class="hidden">1</p>
                        <i class="fa fa-times text-danger"></i>
                        @endif
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>

<script>
$(document).ready(function(){
    $('#users').dataTable();
});
</script>

@stop