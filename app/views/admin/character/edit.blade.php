@extends('layouts.admin.default')
@section('title','Edit '.$character->name)
@section('content')
<div class="row">
    <div class="col-lg-3">
    <h2>Edit {{$character->name}}</h2>
        <div class="panel panel-default margin-top">
            <div class="panel-body">
               {{ Form::open(array('url' => 'admin/character/'.$character->id.'/edit', 'files' => true)) }}
                   <div class='form-group'>
                       {{ Form::label('name', Lang::get('editCharacter.name')); }}
                       {{ Form::text('name', $character->name, ['placeholder' => 'Ex. Raistlin Majere', 'class' => 'form-control']) }}
                   </div>
                   <div class='form-group'>
                       {{ Form::label('level', Lang::get('editCharacter.level')); }}
                       {{ Form::number('level', $character->level, ['placeholder' => 'Ex. 80', 'class' => 'form-control']) }}
                   </div>
                   <div class="form-group">
                   @if($character->sex == 0)
                        {{ Form::radio('sex', '0', true); }}
                        {{ Form::label('sex', Lang::get('editCharacter.female')); }}
                        </br>
                        {{ Form::radio('sex', '1'); }}
                        {{ Form::label('sex', Lang::get('editCharacter.male')); }}
                   @else
                        {{ Form::radio('sex', '0'); }}
                        {{ Form::label('sex', Lang::get('editCharacter.female')); }}
                        </br>
                        {{ Form::radio('sex', '1', true); }}
                        {{ Form::label('sex', Lang::get('editCharacter.male')); }}
                   @endif
                   </div>
                   <div class='form-group'>
                        {{ Form::label('race', Lang::get('editCharacter.race')); }}
                        </br>
                        {{ Form::select('race', DataHelper::getRaces(), $character->race->id); }}
                   </div>
                   <div class='form-group'>
                        {{ Form::label('profession', Lang::get('editCharacter.profession')); }}
                        </br>
                        {{ Form::select('profession', DataHelper::getProfessions(), $character->profession->id); }}
                   </div>
                   <div class='form-group'>
                        {{ Form::label('order', Lang::get('editCharacter.order')); }}
                        </br>
                        {{ Form::select('order', DataHelper::getOrders(), $character->order->id); }}
                   </div>
                   <div class='form-group'>
                   @if($character->world_completion == 1)
                        {{ Form::checkbox('worldcompletion', '1', true); }}
                        {{ Form::label('worldcompletion', Lang::get('editCharacter.worldCompletion')); }}
                   @else
                        {{ Form::checkbox('worldcompletion', '1'); }}
                        {{ Form::label('worldcompletion', Lang::get('editCharacter.worldCompletion')); }}
                   @endif
                        <br>
                   @if($character->personal_story == 1)
                        {{ Form::checkbox('personalstory', '1', true); }}
                        {{ Form::label('personalstory', Lang::get('editCharacter.personalStory')); }}
                   @else
                        {{ Form::checkbox('personalstory', '1'); }}
                        {{ Form::label('personalstory', Lang::get('editCharacter.personalStory')); }}
                   @endif
                   </div>
                   <div class='form-group'>
                        {{ Form::label('fractallevel', Lang::get('editCharacter.fractalLevel')); }}
                        {{ Form::number('fractallevel', $character->fractal_level, ['placeholder' => 'Ex. 21', 'class' => 'form-control fractal-level']) }}
                   </div>
                   <div class='form-group'>
                        {{ Form::label('image', Lang::get('editCharacter.uploadImage')); }}
                        {{ Form::file('image'); }}
                   </div>
                   <div class='form-group'>
                       {{ Form::submit(Lang::get('editCharacter.editCharacter'), ['class' => 'btn btn-lg btn-block custom-color']) }}
                   </div>
                   {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
    @if (isset($name))
        <div class='alert alert-success'>{{ $name }} 'hasBeenSuccessfullyEdited' => 'has been successfully edited.',</div>
        @endif
    </div>
</div>
@stop