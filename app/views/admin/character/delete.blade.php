@extends('layouts.admin.default')
@section('title','Delete '.$character->name)
@section('content')

<div class="row">
    <div class="col-lg-12 text-center">
        <h2>Are you sure you want to delete your character?</h2>
        <h1 class="character-name">{{$character->name}}</h1>
        <br>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 text-center">
        {{ Form::open(array('url' => 'admin/character/'.$character->id.'/delete', 'method' => 'post')) }}
        <div class='form-group'>
        {{ Form::submit('Delete', ['class' => 'btn btn-lg btn-block custom-color delete-button']) }}
        </div>
        {{ Form::close() }}
    </div>
</div>

@stop