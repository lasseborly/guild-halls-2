@extends('layouts.admin.blank')
@section('content')

<div class="container">
        <div class="row">
             <div class="col-md-4 col-md-offset-4">
             <div style="background-image: url({{DataHelper::getURLGuildLogo('500')}});" id="guild-emblem"></div>
             </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-body">
                       {{ Form::open(array('url' => 'register')) }}
                           <div class='form-group'>
                               {{ Form::text('displayname', null, ['placeholder' => Lang::get('register.displayname'), 'class' => 'form-control']) }}
                           </div>
                           <hr>
                           <div class='form-group'>
                               {{ Form::text('email', null, ['placeholder' => Lang::get('register.email'), 'class' => 'form-control']) }}
                           </div>
                           <div class='form-group'>
                                {{ Form::text('emailrepeat', null, ['placeholder' => Lang::get('register.emailrepeat'), 'class' => 'form-control']) }}
                           </div>
                           <hr>
                           <div class='form-group'>
                               {{ Form::password('password', ['placeholder' => Lang::get('register.password'), 'class' => 'form-control']) }}
                           </div>
                           <div class='form-group'>
                               {{ Form::password('passwordrepeat', ['placeholder' => Lang::get('register.passwordrepeat'), 'class' => 'form-control']) }}
                           </div>

                           <div class='form-group'>
                               {{ Form::submit(Lang::get('register.signup'), ['class' => 'btn btn-lg btn-block custom-color']) }}
                           </div>
                           {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
            @if ($errors->has())
                    @foreach ($errors->all() as $error)
                        <div class='alert alert-danger'>{{ $error }}</div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@stop


