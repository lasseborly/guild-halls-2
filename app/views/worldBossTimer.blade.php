@extends('layouts.blank')
@section('title', 'World Boss Timer')
@section('content')
<div class="container page-container">

<script>
  var local = new Date();

  var d = ({{$worldBossTimes[0]->hour}} * 60);


  console.info(14+local.getTimezoneOffset() );


</script>
    <div class="table-responsive">
        <table  class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>Time UTC</th>
                    <th>Countdown</th>
                    <th>World Boss</th>
                    <th>Zone</th>
                    <th>Area</th>
                    <th>Waypoint</th>
                </tr>
            </thead>
            <tbody>
            @if(count($worldBossTimes) > 0)
                @foreach($worldBossTimes as $worldBossTime)
                    <tr>
                        <td>{{sprintf('%02d',$worldBossTime->hour); }}:{{sprintf('%02d',$worldBossTime->minute); }}</td>
                        <td></td>
                        <td>{{WorldBoss::find($worldBossTime->worldBoss_id)->name}}</td>
                        <td>{{WorldBoss::find($worldBossTime->worldBoss_id)->zone}}</td>
                        <td>{{WorldBoss::find($worldBossTime->worldBoss_id)->area}}</td>
                        <td>{{WorldBoss::find($worldBossTime->worldBoss_id)->waypoint}}</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
</div>
@stop