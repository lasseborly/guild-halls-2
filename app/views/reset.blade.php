@extends('layouts.admin.blank')
@section('content')
<div class="container">
        <div class="row">
             <div class="col-md-4 col-md-offset-4">
             <div id="guild-emblem"></div>
             </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">

                    <div class="panel-body">
                       {{ Form::open(array('url' => 'reset')) }}
                           <div class='form-group'>
                               {{ Form::text('email', null, ['placeholder' => Lang::get('reset.email'), 'class' => 'form-control']) }}
                           </div>
                           <div class='form-group reset-button'>
                               {{ Form::submit(Lang::get('reset.resetpassword'), ['class' => 'btn btn-lg btn-block custom-color']) }}
                           </div>
                           {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
