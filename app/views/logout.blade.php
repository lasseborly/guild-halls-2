<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Fairytale Knights(Guild Name)</title>
<link href="{{ asset(adminmain.css) }}" rel="stylesheet" media="screen, projection">
</head>
</html>
<body>
    <p>{{ Lang::get('logout.confirmation' )}} <a href="{{ URL::to('/login') }}">{{Lang::get('logout.login')}}</a></p>
</body>