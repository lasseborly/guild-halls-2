@extends('layouts.blank')
@section('title', Guild::find(1)->name)
@section('content')
@if($page->homepage == 1)
<header style="background-image: url('http://guildhalls2.dev/upload/guild/header.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="intro-text">
                    <div style="background-image: url({{DataHelper::getURLGuildLogo('500')}})" class="logo">
                        <div class="name">{{Guild::find(1)->name}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
@endif

<div class="page-container">
{{$page->content}}
</div>



@stop

