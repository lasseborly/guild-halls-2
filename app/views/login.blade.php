@extends('layouts.admin.blank')
@section('title', 'Login')
@section('content')
<div class="container">
        <div class="row">
             <div class="col-md-4 col-md-offset-4">
             <div style="background-image: url({{DataHelper::getURLGuildLogo('500')}});" id="guild-emblem"></div>
             </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">

                    <div class="panel-body">
                       {{ Form::open(array('url' => 'login', 'method' => 'post')) }}
                           <div class='form-group'>
                               {{ Form::text('email', null, ['placeholder' => Lang::get('login.email'), 'class' => 'form-control']) }}
                           </div>
                           <div class='form-group'>
                               {{ Form::password('password', ['placeholder' => Lang::get('login.password'), 'class' => 'form-control']) }}
                           </div>
                           <div class='form-group login-button'>
                               {{ Form::submit(Lang::get('login.login'), ['class' => 'btn btn-lg btn-block custom-color']) }}
                           </div>
                           <!--
                           <div class="login-checkbox">
                               {{ Form::checkbox('remember', true) }}
                               {{ Form::label('remember',  Lang::get('login.rememberme')) }}
                               <a class="pull-right" href="{{ URL::route('reset');  }}">{{ Lang::get('login.forgotpassword') }}</a>
                           </div> -->
                       {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop