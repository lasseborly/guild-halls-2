@extends('layouts.blank')
@section('title', 'Dungeon Master')
@section('content')

<div class="container page-container">
    {{ Form::open(array('url' => 'dungeonMaster')) }}
    <ul class="list-group dungeons">
            @foreach($dungeons as $dungeon)

            <li style="background-image: url({{ URL::asset('_images/dungeons/'.$dungeon->tag.'.jpg') }})" class="list-group-item dungeon">
                <div class="row">
                    <div class="col-lg-12">
                        <h1>{{$dungeon->name}} ~ {{$dungeon->tag}}</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <div class="bg">
                            <h2>Story</h2>
                            {{ Form::checkbox($dungeon->tag, '1', null, ['class' => 'dungeon-checkbox']); }}
                            <b>{{$dungeon->description}}</b>
                            <h2>Explorable</h2>
                            @foreach($dungeon->paths as $path)
                            {{ Form::checkbox($dungeon->tag.$path->tag, '1', null, ['class' => 'dungeon-checkbox '.$dungeon->tag]); }}
                           <b>Path {{$path->tag}}</b> - {{$path->name}} {{$path->description}}<br>
                            @endforeach
                        </div>
                    <!--
                    </div>
                    <div class="col-md-5">
                       <div class="row">
                            <div class="col-lg-12">
                                <div class="bg-right-top">
                                    <p>Test</p>
                                </div>
                            </div>
                       </div>
                       <div class="row">
                            <div class="col-lg-12">
                                <div class="bg-right-bottom">
                                    <button type="button" class="btn btn-default  button-exp {{$dungeon->id}}"><i class="fa fa-pencil-square-o"></i> Explorable</button>
                                    <script>
                                        $(function () {
                                            $('{{'.'.$dungeon->id}}').on('click', function () {
                                                $('{{'.'.$dungeon->tag}}').each(function(){ this.checked = true; });
                                            });
                                        });
                                    </script>
                                </div>
                            </div>
                       </div>
                    </div>
                </div>
                -->
            </li>
            @endforeach
    </ul>
</div>

@if(Auth::check())
<div class="button-menu">
    <button type="button" class="btn btn-default check">Check all</button>
    <button type="button" class="btn btn-default clear">Clear all</button>
    {{ Form::submit('Save', ['class' => 'btn btn-default save']) }}
    {{ Form::close() }}
</div>
@endif

<script>
    $(function () {
        $('.check').on('click', function () {
            $('.dungeon-checkbox').each(function(){ this.checked = true; });
        });
    });

    $(function () {
        $('.clear').on('click', function () {
            $('.dungeon-checkbox').each(function(){ this.checked = false; });
        });
    });
</script>
@stop