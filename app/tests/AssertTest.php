<?php

class AssertTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function testAssert()
	{
		$theTruth = true;
        $this->assertFalse($theTruth);
	}

    public function testSame()
    {
        $theString = 'Guild Halls 2';
        $this->assertSame('Guild Halls 2', $theString);
    }

    public function testContains()
    {
        $theString = 'Guild Halls 2';
        $this->assertContains('Guild', $theString);
    }

    public function testArrayHasKey()
    {
        $this->assertArrayHasKey('myKey', array('myKey' => 'myArray'));
    }

}
