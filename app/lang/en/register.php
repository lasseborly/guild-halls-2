<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Register Form Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    |
    |
    */

    'email' => 'Email',

    'emailrepeat' => 'Repeat Email',

    'displayname'     => 'Display Name',

    'password'     => 'Password',

    'passwordrepeat' => 'Repeat Password',

    'signup'     => 'Sign Up',

);