<?php

return array(

    'edit' => 'Edit',
    'displayName' => 'Display Name',
    'email' => 'E-mail',
    'password' => 'Password',
    'role' => 'Role',
    'rank' => 'Rank',
    'uploadImage' => 'Upload image',
    'active' => 'Active',
    'achievementPoints' => 'Achievement Points',
    'editUser' => 'Edit User',
    'hasBeenSuccessfullyEdited' => 'has been successfully edited.',

);