<?php

return array(
    'name' => 'Name',
    'edit' => 'Edit',
    'level' => 'Level',
    'race' => 'Race',
    'profession' => 'Profession',
    'worldCompletion' => 'World Completion',
    'order' => 'order',
    'personalStory' => 'Personal Story',
    'fractalLevel' => 'Fractal Level',
    'uploadImage' => 'Upload image',
    'editCharacter' => 'Edit Character',
    'female' => 'Female',
    'male' => 'Male',
    'hasBeenSuccessfullyEdited' => 'has been successfully edited.',

);