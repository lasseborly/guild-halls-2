<?php

return array(

    'guildMembers' => 'Guild Members',
    'rank' => 'Rank',
    'displayName' => 'Display Name',
    'achievementPoints' => 'Achievement Points',
    'email' => 'E-mail',
    'role' => 'Role',
    'createdAt' => 'Created at',
    'active' => 'Active',

);