<?php

return array(

    'users' => 'Users',
    'edit' => 'Edit',
    'level' => 'Level',
    'race' => 'Race',
    'profession' => 'Profession',
    'worldCompletion' => 'World Completion',
    'order' => 'order',
    'personalStory' => 'Personal Story',
    'delete' => 'Delete',

);