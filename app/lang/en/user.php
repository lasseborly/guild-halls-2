<?php

return array(

    'users' => 'Users',
    'edit' => 'Edit',
    'characters' => 'Characters',
    'level' => 'Level',
    'race' => 'Race',
    'profession' => 'Profession',
    'newCharacter' => 'New character',

);