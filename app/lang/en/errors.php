<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Errors Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    |
    |
    */

    'missingemail' => 'Would you mind putting your email in the two small boxes? Then you will be able to login and get all the info.',

    'notsameemail' => 'It seems that the provided email addresses did not match. We are sorry for the inconvenience. Would you please try again?',

    'notanemail' => 'Oi mate. That is not a proper email address! Please input the real deal.',

    'missingdisplayname'     => 'There does not seem to be a display name typed out in the box. How about trying again? For old times sake.',

    'missingpassword'     => 'Security my friend, starts with a password! Just any please. We are missing it. So please put it in the two boxes.',

    'notsamepassword' => 'Sorry, we could not get your two passwords to match. Would you mind trying again?',

    'notadisplayname' => 'That is not a Display Name young Tyrian! Your Display Name can be found at https://account.guildwars2.com/account',


);