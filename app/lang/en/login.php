<?php

return array(

    'loginQuestion' => 'Please Sign In',

    'email' => 'Email',

    'password' => 'Password',

    'login' => 'Login',

    'forgotpassword' => 'Forgot your password?',

    'rememberme' => 'Remember me',

);