<?php

return array(

    'guildView' => 'Guild View',
    'guildMembers' => 'Guild Members',
    'allGuildMembers' => 'All guild members',
    'addNew' => 'Add new',
    'tools' => 'Tools',
    'dungeonMaster' => 'Dungeoon Master',
    'worldBossTimer' => 'World Boss Timer',
    'eventPlanner' => 'Event Planner',
    'pages' => 'Pages',
    'allPages' => 'All pages',
    'style' => 'Style',
    'settings' => 'Settings',
    'profile' => 'Profile',
    'logOut' => 'Log Out',

);