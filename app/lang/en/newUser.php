<?php

return array(

    'newUser' => 'New User',
    'displayName' => 'Display Name',
    'email' => 'E-mail',
    'password' => 'Password',
    'role' => 'Role',
    'rank' => 'Rank',
    'uploadImage' => 'Upload image',
    'createUser' => 'Create User',

);