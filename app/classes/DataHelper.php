<?php
class DataHelper {

    public static function getRanks() {

        $ranks_data       = Rank::all();
        $ranks            = array();
        foreach($ranks_data as $rank)
        {
            $ranks[$rank->id] = $rank->rank;
        }

        return $ranks;
    }

    public static function getRoles() {

        $roles_data = Role::all();
        $roles      = array();
        foreach($roles_data as $role)
        {
            $roles[$role->id] = $role->role;
        }

        return $roles;
    }

    public static function getProfessions() {

        $professions_data = Profession::all();
        $professions      = array();
        foreach($professions_data as $profession)
        {
            $professions[$profession->id] = $profession->profession;
        }

        return $professions;
    }

    public static function getRaces() {

        $races_data       = Race::all();
        $races            = array();
        foreach($races_data as $race)
        {
            $races[$race->id] = $race->race;
        }

        return $races;
    }

    public static function getOrders() {

        $orders_data       = Order::all();
        $orders            = array();
        foreach($orders_data as $order)
        {
            $orders[$order->id] = $order->order;
        }

        return $orders;
    }

    public static function getURLGuildLogo($size) {

        $guildname = Guild::find(1)->name;

        $guildname = strtolower($guildname);
        //Make alphanumeric (removes all other characters)
        $guildname = preg_replace("/[^a-z0-9_\s-]/", "", $guildname);
        //Clean up multiple dashes or whitespaces
        $guildname = preg_replace("/[\s-]+/", " ", $guildname);
        //Convert whitespaces and underscore to dash
        $guildname = preg_replace("/[\s_]/", "-", $guildname);

        $guildlogo = 'http://guilds.gw2w2w.com/guilds/'.$guildname.'/'.$size.'.svg';

        return $guildlogo;
    }
}


