<?php

class Comment extends Eloquent {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'comments';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function creatorUser()
    {
        return $this->belongsTo('User', 'creator_id');
    }


}
