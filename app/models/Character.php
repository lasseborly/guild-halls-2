<?php

class Character extends Eloquent {






    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'characters';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */


    public function user()
    {
        return $this->belongsTo('User');
    }

    public function profession()
    {
        return $this->belongsTo('Profession');
    }

    public function race()
    {
        return $this->belongsTo('Race');
    }

    public function order()
    {
        return $this->belongsTo('Order');
    }

}
