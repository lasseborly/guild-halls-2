<?php

class WorldBossTime extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'worldbosstimes';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    public function worldBoss()
    {
        return $this->belongsTo('WorldBoss');
    }
}
