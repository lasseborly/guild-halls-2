<?php

class Page extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pages';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

}
