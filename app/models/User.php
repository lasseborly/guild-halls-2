<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {




	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');


    public function rank()
    {
        return $this->belongsTo('Rank');
    }

    public function role()
    {
        return $this->belongsTo('Role');
    }

    public  function characters()
    {
        return $this->hasMany('Character');
    }

    public function comments() {

        return $this->hasMany('Comment')->orderBy('created_at', 'desc');
    }

    public function creatorComments() {

        return $this->hasMany('Comment', 'creator_id');
    }





}
