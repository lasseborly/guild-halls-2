#Guild Halls 2

######A guild management app specifically tailored for the game [Guild Wars 2](https://www.guildwars2.com/en/) and it's players.   

Created as an exam project and as the leader of a guild of nearly 300 players. 

##Technology

* ###[Laravel](http://laravel.com/)
     
    PHP web application framework that accelerates development and has both user authentication and MVC structure built into it's core.    
***

* ###[Bootstrap](http://getbootstrap.com/)

    Front-end framework used for it's grid, responsive capabilities and UI elements. 
***

* ###[MySQL](https://www.mysql.com/)

    Relational database that was utilized with Laravels Eloquent ORM which made Modelling very easy.
***